# Generated by Django 4.0 on 2021-12-26 21:49

import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0004_post_content_alter_post_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='content2',
            field=ckeditor.fields.RichTextField(default='rr'),
            preserve_default=False,
        ),
    ]
