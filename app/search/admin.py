from django.contrib import admin
from .models import Post, Category
from mptt.admin import DraggableMPTTAdmin, TreeRelatedFieldListFilter
from django.utils.safestring import mark_safe


class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'parent', 'url', 'button')
    list_filter = (
        ('parent', TreeRelatedFieldListFilter),
    )
    search_fields = ['title']
    readonly_fields = ('url', )

    def button(self, obj):
        return mark_safe(f'<a class="button" href={obj.url} >Visit </a>')


admin.site.register(Post, PostAdmin)
admin.site.register(Category, DraggableMPTTAdmin)
