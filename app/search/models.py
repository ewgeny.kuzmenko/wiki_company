from ckeditor.fields import RichTextField
from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
import requests
from telegraph import Telegraph
from django.conf import settings

telegraph = Telegraph(access_token=settings.TELEGRAPH_TOKEN)


class Category(MPTTModel):
    name = models.CharField(max_length=50, unique=True, blank=True, default="12")
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')

    class MPTTMeta:
        order_insertion_by = ['name']

    def __str__(self):
        return str(self.name)


class Post(models.Model):
    parent = models.ForeignKey(Category, on_delete=models.CASCADE)
    title = models.CharField(max_length=10)
    content = RichTextField()
    url = models.CharField(max_length=10)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        response = telegraph.create_page(
            self.title, author_name='jekajeka63',
            html_content=self.content
        )
        self.url = response['url']
        super(Post, self).save(force_insert, force_update, using, update_fields)
